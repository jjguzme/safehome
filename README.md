# Actualizacion de aplicacion web SafehomeCR #

### Integrantes

* Juan José Guzmán Meza
* José Ulloa Meza
* Alex Navarro Coto
* Gerson Tercero Galo

### Descripción del proyecto

Este proyecto se trata de una aplicación web para la empresa SafeHomeCR. Esta aplicación cuenta con distintos módulos para la creación y administración de tickets de servicio y venta de equipos de seguridad. Esta aplicación ha sido desarrollada en el lenguaje de programación Node.js, además del uso de Bootstrap y jQuery para el FrontEnd. 


### ¿Cómo instalar el repositorio en el equipo?

Antes de instalar el repositorio se debe de cumplir con los siguientes requisitos de software:

* Node.js: Preferiblemente en su versión más reciente.
* Git: Preferiblemente en su versión más reciente.
* Navegador web: El de preferencia, preferiblemente en su versión más reciente.
* Sistema Operativo: La aplicación ha sido desarrollada con el objetivo de ser multiplataforma por lo tanto Windows, Linux y macOS deberían funcionar.
* Editor de texto: Es opcional, en caso de requerir revisar o alterar el código.
* Adicionalmente se debe contar con una cuenta de BitBucket para la clonación del repositorio.

**Pasos para la instalación del repositorio**

1. Se deberá ingresar al repositorio de la aplicación en BitBucket. 
2. Se deberá dar click en la opción de 'clonar' y se procederá a copiar el URL que aparece en pantalla.
3. Se inicia una terminal de linea de comandos en la ruta de la carpeta donde se desea realizar la clonación y se procede a pegar el URL obtenido en el paso 2.
4. Una vez terminada la clonación, en la carpeta del repositorio se deberá digitar el comando 'npm install' en la linea de comandos. Esto con el objetivo de instalar todas las dependencias necesarias para correr la aplicación.
5. Una vez instaladas las dependencias, en la carpeta del repositorio se ingresa el comando 'npm start' en la linea de comandos. Esto pondrá a correr la aplicación.
6. Por último, se ingresa el URL 'localhost:3000' en el navegador web para visualizar la app.